Compile/Build 
=============

## Get Code

```bash
hg clone https://daffodil@bitbucket.org/mjsousa/matiec
```

or using git
```bash
git clone "hg::https://daffodil@bitbucket.org/mjsousa/matiec"
```



## Compiling under Linux

```bash
autoreconf -i
./configure
make
```

## Cross-Compiling under Linux, for Windows
```bash
./configure  --host=i586-pc-mingw32
```
(or, to use static linking, which does not require installing any mingw dll's on windows)
```bash
./configure  --host=i586-pc-mingw32 LDFLAGS="-static"
make
```




## Maintaining the Build Environment

-> Add new files to Makefile.am or add a new makefile
```bash
autoreconf
```

-> Prepare clean project
```bash
make distclean
```


-> Remember to add these files to the .hgignore or .gitignore
```bash
Makefile
config.*
*.a
.deps
```

-> Send me TODO list to complete build system.
```bash
matteo.facchinetti@sirius-es.it
```
